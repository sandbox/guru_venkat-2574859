jQuery(document).ready(function() {
	var btnclone	= jQuery('#edit-submit').clone().addClass('guvefloatingbtn');
	
	if(jQuery('#block-admin-display-form').length)
		jQuery('#block-admin-display-form').append(btnclone);
	
	if(jQuery('#system-modules').length)
		jQuery('#system-modules').append(btnclone);
		
	var offset = 0;
	var duration = 500;
	jQuery(window).scroll(function() {
		if (jQuery(this).scrollTop() > offset ) {
			jQuery('.guvefloatingbtn').fadeIn(duration);
		} else {
			jQuery('.guvefloatingbtn').fadeOut(duration);
		}
		if ((jQuery(window).scrollTop() + jQuery(window).height()) == jQuery(document).height()) {
            jQuery('.guvefloatingbtn').fadeOut(duration);
		}
	});
});