Description
-----------
This module will help the administartor or developer to save the configuration or block 
without scrolling down to get the configuration button whch is normally placed at the bootom of the page.
The configuration button will be seen as floating at the right upperside of the page.
The floating configuration button will be seen in the following pages after enabling it:
1) Modules list page
2) Block configuration page

Requirements
------------
Drupal 7.x
See https://www.drupal.org/project/floatingbtn for additional requirements.

Installation
------------
1. Copy the entire floatingbtn directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"


Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/floatingbtn
